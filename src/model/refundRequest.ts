import { Url } from "./url";

export class RefundRequest {
  /** @description Valor do cancelamento sem separador de milhar e casa decimal. Ex.: R$ 10,00 = 1000 | R$ 0,50 = 50 */
  amount: number;

  constructor(data: any = {}) {
    this.amount = data.amount || 0;
  }
}